package ru.dmpas.onescript.eclipseplugin.editors;

public interface IUniwordDetector {

	/**
	 * Returns whether the specified character is
	 * valid as the first character in a word.
	 *
	 * @param c the character to be checked
	 * @return <code>true</code> is a valid first character in a word, <code>false</code> otherwise
	 */
	boolean isWordStart(int c);

	/**
	 * Returns whether the specified character is
	 * valid as a subsequent character in a word.
	 *
	 * @param c the character to be checked
	 * @return <code>true</code> if the character is a valid word part, <code>false</code> otherwise
	 */
	boolean isWordPart(int c);

}
