package ru.dmpas.onescript.eclipseplugin.editors;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.EndOfLineRule;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;

public class OneScriptScanner extends RuleBasedScanner {

	private final String OrdinalWordTokenData = "__tk_Word"; //$NON-NLS-1$
	
	
	private class Detector implements IWordDetector {

		@Override
		public boolean isWordStart(char c) {
			return Character.isAlphabetic(c);
		}

		@Override
		public boolean isWordPart(char c) {
			return Character.isAlphabetic(c) || Character.isDigit(c);
		}
		
	}
	
	protected void initWords(WordRule words, ColorManager manager)
	{
		final Token Keyword = new Token(new TextAttribute(manager.getColor(DefaultColorConstants.KEYWORD), null, SWT.BOLD));
		
		final String keywords[] = new String [] {
				"если", 				//$NON-NLS-1$
				"тогда", 				//$NON-NLS-1$
				"иначе",				//$NON-NLS-1$
				"иначеесли", 			//$NON-NLS-1$
				"конецесли", 			//$NON-NLS-1$
				"пока", 				//$NON-NLS-1$
				"цикл", 				//$NON-NLS-1$
				"конеццикла", 			//$NON-NLS-1$
				"для", 					//$NON-NLS-1$
				"каждого", 				//$NON-NLS-1$
				"из", 					//$NON-NLS-1$
				"процедура", 			//$NON-NLS-1$
				"конецпроцедуры", 		//$NON-NLS-1$
				"функция", 				//$NON-NLS-1$
				"конецфункции", 		//$NON-NLS-1$
				"знач", 				//$NON-NLS-1$
				"экспорт", 				//$NON-NLS-1$
				"неопределено", 		//$NON-NLS-1$
				"вызватьисключение", 	//$NON-NLS-1$
				"попытка", 				//$NON-NLS-1$
				"исключение", 			//$NON-NLS-1$
				"конецпопытки", 		//$NON-NLS-1$
				"перем", 				//$NON-NLS-1$
				// TODO: провѣрить полноту списка
				// TODO: вбить англійскія слова
				"null" 					//$NON-NLS-1$
		};
		
		for (int i = 0; i < keywords.length; i++)
			words.addWord(keywords[i], Keyword);
	}
	
	public OneScriptScanner(ColorManager manager) {
		
		final Token Word = new Token(OrdinalWordTokenData);
		
		WordRule words = new WordRule(new Detector(), Word, true);

		initWords(words, manager);
		
		final Token Comment = new Token(new TextAttribute(manager.getColor(DefaultColorConstants.COMMENT)));
		final EndOfLineRule comments = new EndOfLineRule("//", Comment);
		
		final Token Preproc = new Token(new TextAttribute(manager.getColor(DefaultColorConstants.PROC_INSTR)));
		final EndOfLineRule preproc = new EndOfLineRule("#", Preproc);
		
		setRules(new IRule[] {words, comments, preproc});
	}
}
