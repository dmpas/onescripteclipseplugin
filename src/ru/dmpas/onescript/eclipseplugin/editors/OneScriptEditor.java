package ru.dmpas.onescript.eclipseplugin.editors;

import org.eclipse.ui.editors.text.TextEditor;

public class OneScriptEditor extends TextEditor {

	private ColorManager colorManager;

	public OneScriptEditor() {
		super();
		colorManager = new ColorManager();
		setSourceViewerConfiguration(new OneScriptConfiguration(colorManager));
	}
	public void dispose() {
		colorManager.dispose();
		super.dispose();
	}

}
